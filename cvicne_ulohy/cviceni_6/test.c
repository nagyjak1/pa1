#ifndef __PROGTEST__
#include <stdio.h>
#include <assert.h>
#endif /* __PROGTEST__ */

int isLeap ( int year ) {
    int isLeap = 0;
    if ( year % 4000 == 0 )
        isLeap = 0;
    else if ( year % 400 == 0)
        isLeap = 1;
    else if ( year % 100 == 0)
        isLeap = 0;
    else if ( year % 4 == 0 )
        isLeap = 1;
    return isLeap;
}

int monthToDate ( int month, int year ) {
    int days = 0;
    switch ( month ) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            days = 31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            days = 30;
            break;
        case 2:
            if ( isLeap (year) )
                days = 29;
            else
                days = 28;
            break;
    }
    return days;
}

int dateToIndex(int day, int month, int year, int *idx) {
    if ( year < 2000 || month < 1 || month > 12 || day < 1 || day > monthToDate(month, year) )
        return 0;

    int index = 0;
    index += day;
    for ( int i = 1; i < month; ++i )
        index += monthToDate( i, year );
    *idx = index;
    return 1;
}

#ifndef __PROGTEST__
int main (int argc, char * argv []) {
    int idx;
    assert(isLeap(2000));
    assert(isLeap(2004));
    assert(isLeap(2104));
    assert(isLeap(2096));
    assert(!isLeap(2001));
    assert(!isLeap(2005));
    assert(!isLeap(2100));
    assert(!isLeap(2300));
    assert(isLeap(2400));
    assert(isLeap(3600));
    assert(isLeap(4400));
    assert(!isLeap(4000));
    assert(!isLeap(8000));

    assert(dateToIndex( 1,  1, 2000, &idx) == 1 && idx == 1);
    assert(dateToIndex( 1,  2, 2000, &idx) == 1 && idx == 32);
    assert(dateToIndex(29,  2, 2000, &idx) == 1 && idx == 60);
    assert(dateToIndex(29,  2, 2001, &idx) == 0);
    assert(dateToIndex( 1, 12, 2000, &idx) == 1 && idx == 336);
    assert(dateToIndex(31, 12, 2000, &idx) == 1 && idx == 366);
    assert(dateToIndex( 1,  1, 1999, &idx) == 0);
    assert(dateToIndex( 6,  7, 3600, &idx) == 1 && idx == 188);
    assert(dateToIndex(29,  2, 3600, &idx) == 1 && idx == 60);
    assert(dateToIndex(29,  2, 4000, &idx) == 0);
    return 0;
}
#endif /* __PROGTEST__ */

