#include <stdio.h>

int loadInput(int *n, int *k) {
    printf("Pocet cifer:\n");
    if (scanf("%d", n) != 1 || *n <= 0 || *n > 64)
        return 0;
    printf("Max. stejnych po sobe:\n");
    if (scanf("%d", k) != 1 || *k <= 0)
        return 0;
    return 1;
}

int isRepeating(const char *binNumber, int maxRepeating, int index) {
    int zerosCounter = 0, onesCounter = 0;

    for (int i = 0; i < index; ++i) {
        if (binNumber[i] == '1') {
            zerosCounter = 0;
            onesCounter++;
        } else if (binNumber[i] == '0') {
            zerosCounter++;
            onesCounter = 0;
        }
        if (zerosCounter > maxRepeating || onesCounter > maxRepeating)
            return 1;
    }
    return 0;
}

void generateBinaryNumbers(int length, int maxRepeating, char *binNumber, int index, int *counter) {

    if (isRepeating(binNumber, maxRepeating, index))
        return;

    if (length == 0) {
        if (binNumber[0] == '0')
            return;
        printf("%s\n", binNumber);
        (*counter)++;
        return;
    }

    binNumber[index] = '0';
    generateBinaryNumbers(length - 1, maxRepeating, binNumber, index + 1, counter);

    binNumber[index] = '1';
    generateBinaryNumbers(length - 1, maxRepeating, binNumber, index + 1, counter);

    if (binNumber[0] == '0')
        return;

}

int main() {
    int n, k, counter = 0;
    char binNumber[65] = {};

    if (!loadInput(&n, &k)) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    for (int i = 1; i <= n; ++i)
        generateBinaryNumbers(i, k, binNumber, 0, &counter);
    printf("Celkem: %d\n", counter);

    return 0;
}
