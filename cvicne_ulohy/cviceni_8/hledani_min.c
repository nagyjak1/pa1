#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int loadInput(char **field, int *rowLen, int *rowsCount, int *allocatedRowLen, int *allocatedRowsCount) {
    printf("Zadejte hraci plochu:\n");
    int columns = 0, rows = 0;
    int rowSize = 0;
    char c;
    while (1) {
        if (rows >= *allocatedRowsCount) {
            char *largerField = (char *) realloc(*field,
                                                 (*allocatedRowLen) * (*allocatedRowsCount) * 2 * sizeof(**field));
            if (largerField == NULL) {
                printf("REALOKACE SE NEPODARILA\n");
                free(field);
                exit(1);
            }
            *field = largerField;
            for (int i = (*allocatedRowLen) * (*allocatedRowsCount);
                 i < (*allocatedRowLen) * 2 * (*allocatedRowsCount); ++i)
                (*field)[i] = 0;
            (*allocatedRowsCount) = (*allocatedRowsCount) * 2;
        }

        while (1) {

            if (columns >= *allocatedRowLen) {
                char *largerField = (char *) realloc(*field,
                                                     (*allocatedRowLen) * 2 * (*allocatedRowsCount) * sizeof(**field));
                if (largerField == NULL) {
                    printf("REALOKACE SE NEPODARILA\n");
                    free(field);
                    exit(1);
                }
                *field = largerField;
                for (int i = (*allocatedRowLen) * (*allocatedRowsCount);
                     i < (*allocatedRowLen) * 2 * (*allocatedRowsCount); ++i)
                    (*field)[i] = 0;
                (*allocatedRowLen) = (*allocatedRowLen) * 2;
            }

            scanf("%c", &c);
            if (c != '.' && c != '*' && c != '\n')
                return -1;
            if (feof(stdin)) {
                if (columns != 0 || rows < 1)
                    return -1;
                *rowLen = rowSize;
                *rowsCount = rows;
                return 1;
            }
            if (c == '\n') {
                break;
            }
            (*field)[rows * rowSize + columns] = c;
            columns++;
        }
        if (columns == 0)
            return -1;
        if (rowSize != 0 && columns != rowSize)
            return -1;
        if (rowSize == 0)
            rowSize = columns;
        columns = 0;
        rows++;
    }
}

char minesAround(const char *field, int rowLen, int rowsCnt, int x, int y) {
    char counter = '0';

    if (field[x + y * rowLen] == '*')
        return '*';

    if (x != 0)
        if (field[x - 1 + y * rowLen] == '*')
            counter++;
    if (y != 0)
        if (field[x + (y - 1) * rowLen] == '*')
            counter++;
    if (x != rowLen - 1)
        if (field[x + 1 + y * rowLen] == '*')
            counter++;
    if (y != rowsCnt)
        if (field[x + (y + 1) * rowLen] == '*')
            counter++;

    if (x != 0 && y != 0)
        if (field[x - 1 + (y - 1) * rowLen] == '*')
            counter++;
    if (x != rowLen - 1 && y != rowsCnt)
        if (field[x + 1 + (y + 1) * rowLen] == '*')
            counter++;
    if (x != 0 && y != rowsCnt)
        if (field[x - 1 + (y + 1) * rowLen] == '*')
            counter++;
    if (x != rowLen - 1 && y != 0)
        if (field[x + 1 + (y - 1) * rowLen] == '*')
            counter++;


    if (counter == '0')
        return '.';

    return counter;

}

void printMineField(char *field, int rowLen, int rowsCnt) {
    printf("Vyplnena hraci plocha:\n");
    for (int i = 0; i < rowsCnt; ++i) {
        for (int j = 0; j < rowLen; ++j) {
            printf("%c", minesAround(field, rowLen, rowsCnt, j, i));
        }
        printf("\n");
    }
}

void printMap(char *field, int rowLen, int rowsCnt) {
    printf("Hraci plocha:\n");
    for (int i = 0; i < rowsCnt; ++i) {
        for (int j = 0; j < rowLen; ++j) {
            printf("%c", field[i * rowLen + j]);
        }
        printf("\n");
    }
}

int main(void) {
    int fieldSizeX = 0, fieldSizeY = 0;
    int allocatedRowLen = 100;
    int allocatedRowCnt = 100;

    char *field = (char *) calloc(allocatedRowCnt * allocatedRowLen, sizeof(*field));

    if (loadInput(&field, &fieldSizeX, &fieldSizeY, &allocatedRowLen, &allocatedRowCnt) == -1) {
        printf("Nespravny vstup.\n");
        free(field);
        return -1;
    }

    printMineField(field, fieldSizeX, fieldSizeY);

    free(field);
    return 0;
}