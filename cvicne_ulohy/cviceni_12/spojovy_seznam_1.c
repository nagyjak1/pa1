#ifndef __PROGTEST__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct TItem {
    struct TItem *m_Next;
    int m_Val;
} TITEM;

typedef struct TData {
    TITEM *m_First;
    TITEM *m_Last;
} TDATA;

#endif /* __PROGTEST__ */

void insertStart(TDATA *l, int x) {
    TITEM *newItem = (TITEM *) malloc(sizeof(TITEM));
    newItem->m_Val = x;
    newItem->m_Next = l->m_First;

    if (l->m_Last == NULL) {
        l->m_First = l->m_Last = newItem;
        return;
    }

    l->m_First = newItem;
}

void insertEnd(TDATA *l, int x) {
    TITEM *newItem = (TITEM *) malloc(sizeof(TITEM));
    newItem->m_Val = x;
    newItem->m_Next = NULL;

    if (l->m_Last == NULL) {
        l->m_First = l->m_Last = newItem;
        return;
    }

    l->m_Last->m_Next = newItem;
    l->m_Last = newItem;
}

int findMax(TDATA *l) {
    if (l->m_First == NULL)
        return 0;

    TITEM *ptr = l->m_First;
    int max = ptr->m_Val;

    while (ptr != NULL) {
        if (ptr->m_Val > max)
            max = ptr->m_Val;
        ptr = ptr->m_Next;
    }
    return max;
}

void destroyAll(TDATA *l) {
    TITEM *x = l->m_First;
    while (x != NULL) {
        TITEM *tmp = x->m_Next;
        free(x);
        x = tmp;
    }
    l->m_Last = l->m_First = NULL;
}

int removeMax(TDATA *l) {
    int counter = 0;
    int max = findMax(l);

    TDATA tmp;
    tmp.m_First = tmp.m_Last = NULL;
    TITEM *ptr = l->m_First;

    while (ptr != NULL) {
        if (ptr->m_Val != max)
            insertEnd(&tmp, ptr->m_Val);
        else
            counter++;
        ptr = ptr->m_Next;
    }

    destroyAll(l);
    *l = tmp;
    return counter;
}

#ifndef __PROGTEST__

int main() {
    return 0;
}

#endif /* __PROGTEST__ */