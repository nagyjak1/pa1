#include <stdio.h>
#include <math.h>

enum operation {
    Plus, Minus, Multiply, Divide, Error
};

enum operation loadInput(double *x, double *y) {
    char op, c1, c2, equal, c3;
    printf("Zadejte vzorec:\n");

    if (scanf("%lf%c%c%c%lf%c%c", x, &c1, &op, &c2, y, &c3, &equal) != 7 ||
        c1 != ' ' || c2 != ' ' || c3 != ' ' || equal != '=')
        return Error;
    switch (op) {
        case '+':
            return Plus;
        case '-':
            return Minus;
        case '*':
            return Multiply;
        case '/':
            if (*y == 0)
                return Error;
            return Divide;
        default:
            return Error;
    }
}

int main() {
    double x, y;
    switch (loadInput(&x, &y)) {
        case Error:
            printf("Nespravny vstup.\n");
            return 1;
        case Plus:
            printf("%lg\n", x + y);
            return 0;
        case Minus:
            printf("%lg\n", x - y);
            return 0;
        case Multiply:
            printf("%lg\n", x * y);
            return 0;
        case Divide: {
            x / y > 0 ? printf("%lg\n", floor(x / y)) : printf("%lg\n", round(x / y));
            return 0;
        }
    }
    return 0;
}