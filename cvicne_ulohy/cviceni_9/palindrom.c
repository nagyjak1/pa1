#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <string.h>


int loadInput ( char** string ) {

    int newLineFound = 0;
    size_t lineSize = 1;
    int charsCnt = 0;
    char* line = (char*) malloc ( sizeof(char) * lineSize );

    int x = getline ( &line, &lineSize, stdin);
    char* tmpString = (char*) malloc ( sizeof(char) * (lineSize+1) );

    if ( x == -1 ) {
        (*string) = tmpString;
        free(line);
        return 2;
    }

    for ( size_t i = 0; i < lineSize; ++i) {
        if (line[i] == '\n') {
            newLineFound = 1;
            tmpString[charsCnt] = '\0';
            break;
        }
        if ( ! isspace(line[i]) ) {
            tmpString[charsCnt] = line[i];
            charsCnt++;
        }
    }

    free(line);
    (*string) = tmpString;
    if ( strcmp ((*string), "" ) == 0 || !newLineFound )
        return 0;
    return 1;
}

int isPalindrome ( char* string ) {

    int len =  strlen(string);
    char* front = string;
    char* back = string+(len-1);

    for ( int i = 0; i < ceil(len/2); ++i ) {
        if (*(front + i) != *(back - i))
            return 0;
    }
    return 1;
}

int isPalindromeCaseInsensitive ( char* string ) {
    int len =  strlen(string);
    char* front = string;
    char* back = string+(len-1);

    for ( int i = 0; i < ceil(len/2); ++i ) {
        if (tolower(*(front + i)) != tolower(*(back - i)))
            return 0;
    }
    return 1;
}

int main ( void ) {
    char* string;
    printf("Zadejte retezec:\n");
    while (1) {
        int x = loadInput(&string);

        if ( x == 0) {
            printf("Nespravny vstup.\n");
            free(string);
            return 1;
        }
        if ( x == 2) {
            break;
        }

        if ( isPalindrome(string) )
            printf("Retezec je palindrom (case-sensitive).\n");
        else if ( isPalindromeCaseInsensitive(string) )
            printf("Retezec je palindrom (case-insensitive).\n");
        else
            printf("Retezec neni palindrom.\n");
        free(string);

    }
    free(string);
    return 0;
}
