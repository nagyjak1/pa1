#include <stdio.h>

int readInput(int *x, int *y) {
    printf("Zadejte pocet poli:\n");
    if (scanf("%d", x) != 1 || *x <= 0)
        return 1;
    printf("Zadejte velikost pole:\n");
    if (scanf("%d", y) != 1 || *y <= 0)
        return 1;
    return 0;
}

void printRow(int x, int y, char firstChar, char secondChar) {
    char c;
    printf("|");
    for (int i = 0; i < x; ++i) {
        for (int j = 0; j < y; ++j) {
            if (i % 2 == 1)
                c = secondChar;
            else
                c = firstChar;
            printf("%c", c);
        }
    }
    printf("|\n");
}


void printFirstLastLine(int length) {
    printf("+");
    for (int i = 0; i < length; ++i)
        printf("-");
    printf("+\n");
}


void printBoard(int x, int y, char firstChar, char secondChar) {
    printFirstLastLine(x * y);
    for (int i = 0; i < x; ++i) {
        for (int j = 0; j < y; ++j) {
            if (i % 2 == 1)
                printRow(x, y, secondChar, firstChar);
            else
                printRow(x, y, firstChar, secondChar);
        }
    }
    printFirstLastLine(x * y);
}

int main() {
    int x, y;
    if (readInput(&x, &y) != 0) {
        printf("Nespravny vstup.\n");
        return 1;
    }
    printBoard(x, y, ' ', 'X');

    return 0;
}

