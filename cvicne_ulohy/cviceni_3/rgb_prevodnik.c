#include <stdio.h>

int main ( void ) {

    int a,b,c;
    char ch1, ch2;

    printf("Zadejte barvu v RGB formatu:\n");
    int x = scanf ( " rgb %c %d , %d , %d %c ", &ch1, &a, &b, &c, &ch2 );

    if (x != 5 || a < 0 || b < 0 || c < 0 || a > 255 || b > 255 || c > 255 || ch1 != '(' || ch2 != ')' ) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    printf( "#%02X%02X%02X\n", a, b, c);
    return 0;

}