#include <stdio.h>
#include <math.h>

int ReadTime ( int* h, int* m, int* s, int* ms ) {
    char c;
    *ms = 0;

    int x = scanf ( " %d : %d : %d ,", h, m, s );
    int d = 0;

    while ( scanf("%1c", &c) == 1 ) {
        if (c == ' ')
            continue;
        if (c == '\n') {
            if (d == 0)
                return 1;
            break;
        }
        if (c < '0' || c > '9' )
            return 1;
        *ms += (c - '0') * pow(10,2-d);
        d++;
        if (d > 3)
            return 1;
    }

    if ( x != 3 || *h < 0 || *h > 23 || *m > 59 || *m < 0 || *s > 59 || *s < 0 || *ms > 999 || *ms < 0 )
        return 1;
    return 0;
}

int countIntervals ( void ) {

    int h1, m1, s1, ms1;
    int h2, m2, s2, ms2;

    printf ( "Zadejte cas t1:\n" );
    if ( ReadTime ( &h1, &m1, &s1, &ms1 ) )
        return 1;

    printf ( "Zadejte cas t2:\n" );
    if ( ReadTime ( &h2, &m2, &s2, &ms2 ) )
        return 1;

    long t1, t2, diff;
    int h, m, s, ms;
    t1 = h1*60*60*1000 + m1*60*1000 + s1*1000 + ms1;
    t2 = h2*60*60*1000 + m2*60*1000 + s2*1000 + ms2;

    if ( t1 > t2 )
        return 1;

    diff = t2 - t1;

    ms = diff % 1000;
    diff /= 1000;
    s = diff % 60;
    diff /= 60;
    m = diff % 60;
    diff /= 60;
    h = diff % 60;

    printf ( "Doba: %2d:%02d:%02d,%03d\n", h, m, s, ms );
    return 0;
}

int main ( void ) {
    if ( countIntervals() ) {
        printf("Nespravny vstup.\n");
        return 1;
    }
    return 0;
}