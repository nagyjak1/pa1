#include <stdio.h>

int loadInput(int *n, int *p, int *votes) {

    printf("Pocet stran:\n");
    if (scanf("%d", n) != 1 || *n < 2 || *n > 26)
        return 0;

    printf("Strany a pocet hlasu:\n");
    for (int i = 0; i < *n; ++i) {
        char c;
        int d;
        if (scanf(" %c %d ", &c, &d) != 2 || c < 'A' || c >= 'A' + *n || d < 1 || votes[c - 'A'] != -1)
            return 0;
        votes[c - 'A'] = d;
    }

    printf("Pocet horniku:\n");
    if (scanf("%d", p) != 1 || *p <= 0)
        return 0;

    return 1;
}

//----------------------------------------------------------------------------------------------------------------------

double calcPreferences(int votes, int givenPeople) {
    if (givenPeople != 0)
        return votes * 1.0 / (1 + givenPeople);
    else
        return votes / 1.42;
}

//----------------------------------------------------------------------------------------------------------------------

void findMaxPreferences(int *votes, int parties, int *givenPeople, int *maxParties, int *numberOfMaxParties) {

    double max = calcPreferences(votes[0], givenPeople[0]);
    for (int i = 1; i < parties; ++i) {
        if (max < calcPreferences(votes[i], givenPeople[i]))
            max = calcPreferences(votes[i], givenPeople[i]);
    }

    *numberOfMaxParties = 0;
    for (int i = 0; i < parties; ++i) {
        if (max == calcPreferences(votes[i], givenPeople[i])) {
            maxParties[*numberOfMaxParties] = i;
            (*numberOfMaxParties)++;
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

int divideVotes(int parties, int *votes, int peopleToDivide, int *givenPeople) {
    int numberOfMaxParties;
    int maxParties[26];

    for (int i = 0; i < peopleToDivide;) {
        findMaxPreferences(votes, parties, givenPeople, maxParties, &numberOfMaxParties);

        if (peopleToDivide - i < numberOfMaxParties)
            return 0;
        for (int j = 0; j < numberOfMaxParties; ++j)
            givenPeople[maxParties[j]]++;
        i += numberOfMaxParties;
    }
    return 1;
}

//----------------------------------------------------------------------------------------------------------------------

int main() {
    int numberOfParties, peopleToDivide;
    int votes[26];
    int givenPeople[26];
    for (int i = 0; i < 26; ++i) {
        votes[i] = -1;
        givenPeople[i] = 0;
    }

    if (!loadInput(&numberOfParties, &peopleToDivide, votes)) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    if (!divideVotes(numberOfParties, votes, peopleToDivide, givenPeople))
        printf("Nelze rozdelit.\n");
    else {
        printf("Pridelene pocty:\n");
        for (int i = 0; i < numberOfParties; ++i)
            printf("%c: %d\n", 'A' + i, givenPeople[i]);
    }
    return 0;
}
