#include <stdio.h>
#include <limits.h>
#include <math.h>
#include <float.h>

int readInput(double *fabricHeight, double *fabricWidth, double *sailHeight, double *sailWidth) {
    printf("Velikost latky:\n");
    if (scanf("%lf %lf", fabricWidth, fabricHeight) != 2 || *fabricWidth <= 0 || *fabricHeight <= 0)
        return 1;
    printf("Velikost plachty:\n");
    if (scanf("%lf %lf", sailHeight, sailWidth) != 2 || *sailWidth <= 0 || *sailHeight <= 0)
        return 1;
    return 0;
}

int askForOverlay(double *overlay) {
    printf("Prekryv:\n");
    if (scanf("%lf", overlay) != 1 || *overlay < 0)
        return 1;
    return 0;
}

int mustBeSewed(double fabricHeight, double fabricWidth, double sailHeight, double sailWidth) {
    if ((sailHeight <= fabricHeight && sailWidth <= fabricWidth) ||
        (sailHeight <= fabricWidth && sailWidth <= fabricHeight))
        return 0;
    return 1;
}

int bestLayout(double fabricHeight, double fabricWidth, double sailHeight, double sailWidth, double overhead) {
    if (!mustBeSewed(fabricHeight, fabricWidth, sailHeight, sailWidth))
        return 1;

    if (fabricHeight <= overhead)
        return INT_MAX;

    int totalX = 0, totalY = 0;

    if (sailHeight > fabricHeight) {
        totalY++;
        sailHeight -= fabricHeight;

        if (fabs(sailHeight / (fabricHeight - overhead) - floor(sailHeight / (fabricHeight - overhead))) <
            DBL_EPSILON * fabs(sailHeight / (fabricHeight - overhead) + floor(sailHeight / (fabricHeight - overhead))) )
            totalY += floor(sailHeight / (fabricHeight - overhead));
        else
            totalY += ceil(sailHeight / (fabricHeight - overhead));
    } else
        totalY = 1;

    if (sailWidth > fabricWidth) {
        totalX++;
        sailWidth -= fabricWidth;

        if (fabs(sailWidth / (fabricWidth - overhead) - floor(sailWidth / (fabricWidth - overhead))) <
            DBL_EPSILON * fabs(sailWidth / (fabricWidth - overhead) + floor(sailWidth / (fabricWidth - overhead))))
            totalX += floor(sailWidth / (fabricWidth - overhead));
        else
            totalX += ceil(sailWidth / (fabricWidth - overhead));
    } else
        totalX = 1;

    return totalX * totalY;

}


int main() {
    double fabricHeight, fabricWidth, sailHeight, sailWidth;
    if (readInput(&fabricHeight, &fabricWidth, &sailHeight, &sailWidth) == 1) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    double overhead = 0;
    if (mustBeSewed(fabricHeight, fabricWidth, sailHeight, sailWidth))
        if (askForOverlay(&overhead)) {
            printf("Nespravny vstup.\n");
            return 1;
        }

    int best;
    int x = bestLayout(fabricHeight, fabricWidth, sailHeight, sailWidth, overhead);
    int y = bestLayout(fabricWidth, fabricHeight, sailHeight, sailWidth, overhead);
    if (y < x)
        best = y;
    else
        best = x;

    if (best == INT_MAX)
        printf("Nelze vyrobit.\n");
    else
        printf("Pocet kusu latky: %d\n", best);

    return 0;
}
