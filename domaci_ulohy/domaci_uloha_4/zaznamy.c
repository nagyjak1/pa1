#include <stdio.h>
#include <stdlib.h>

#define MAX_LOGS 1000000
#define MAX_IDS 100000

enum op {
    add, search, error, eof, idle
};
//----------------------------------------------------------------------------------------------------------------------
void swap(int *a, int *b) {
    int t = *a;
    *a = *b;
    *b = t;
}
//----------------------------------------------------------------------------------------------------------------------
int findMedian(int n1, int n2, int n3) {
    int max = n1;
    if (n2 > max)
        max = n2;
    if (n3 > max)
        max = n3;

    int min = n1;
    if (n2 < min)
        min = n1;
    if (n3 < min)
        min = n3;

    return n1 + n2 + n3 - max - min;
}
//----------------------------------------------------------------------------------------------------------------------
int partition(int array[], int low, int high) {
    int pivot = array[high];
    int i = low;
    for (int j = low; j <= high - 1; j++) {
        if (array[j] <= pivot) {
            swap(&array[i], &array[j]);
            i++;
        }
    }
    swap(&array[i], &array[high]);
    return i;
}
//----------------------------------------------------------------------------------------------------------------------
int partition_r(int array[], int low, int high) {
    int r1 = low + (rand() % (high - low + 1));
    int r2 = low + (rand() % (high - low + 1));
    int r3 = low + (rand() % (high - low + 1));
    int median = findMedian(r1, r2, r3);
    swap(&array[median], &array[high]);
    return partition(array, low, high);
}
//----------------------------------------------------------------------------------------------------------------------
void quickSort(int array[], int low, int high) {
    if (low < high) {
        int pivot = partition_r(array, low, high);
        quickSort(array, low, pivot - 1);
        quickSort(array, pivot + 1, high);
    }
}
//----------------------------------------------------------------------------------------------------------------------
int loadInput(int *id, int *from, int *to) {
    char op;
    if (scanf("%c", &op) != 1) {
        if (feof(stdin))
            return eof;
        return error;
    }

    if (op == '+') {
        if (scanf("%d", id) != 1 || *id < 0 || *id >= MAX_IDS) {
            return error;
        }
        return add;
    }
    if (op == '?') {
        if (scanf("%d %d", from, to) != 2 || *from < 0 || *from > *to)
            return error;
        return search;
    }
    if (op != '?' && op != '+' && op != '\n') {
        return error;
    }
    return idle;
}
//----------------------------------------------------------------------------------------------------------------------
int countUniqueIDs(const int array[], const int start, const int end) {
    int uniqueCount = 1;
    int *sortedArray = (int *) calloc(end - start + 1, sizeof(int));
    for (int i = 0; i < end - start + 1; ++i)
        sortedArray[i] = array[start + i];

    quickSort(sortedArray, 0, end - start);

    for (int i = 0; i < end - start; ++i)
        if (sortedArray[i] != sortedArray[i + 1])
            uniqueCount++;

    free(sortedArray);
    return uniqueCount;
}
//----------------------------------------------------------------------------------------------------------------------
int main() {
    int idArray[MAX_IDS];
    int logArray[MAX_LOGS];
    int id, from = 0, to = 0, op;
    int logCounter = 0;
    for (int i = 0; i < MAX_IDS; ++i)
        idArray[i] = 0;

    printf("Pozadavky:\n");
    while (1) {
        op = loadInput(&id, &from, &to);

        if (op == error)
            break;

        if (op == add) {
            if (logCounter >= MAX_LOGS)
                break;

            idArray[id]++;
            logArray[logCounter] = id;
            logCounter++;

            if (idArray[id] == 1)
                printf("> prvni navsteva\n");
            else
                printf("> navsteva #%d\n", idArray[id]);
        }

        if (op == search) {
            if (to >= logCounter)
                break;
            int recordsCount = to - from + 1;
            int uniqueIDCount = countUniqueIDs(logArray, from, to);
            printf("> %d / %d\n", uniqueIDCount, recordsCount);
        }
        if (op == eof)
            return 0;
    }
    printf("Nespravny vstup.\n");
    return 1;
}