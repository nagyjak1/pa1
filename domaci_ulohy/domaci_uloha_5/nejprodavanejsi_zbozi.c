#include <stdio.h>
#include <stdlib.h>
#include "/usr/include/string.h"

enum command { Add, ListAll, ListCnt, Error, Idle, Eof };

typedef struct Product {
    int soldCnt;
    char name[101];
} Product;


int loadNumberOfWatched ( int * n ) {
    printf("Pocet sledovanych:\n");
    if ( scanf ( "%d", n) != 1 || *n <= 0 )
        return 0;
    return 1;
}

int loadCommand ( char* productName ) {
    char op;
    if (scanf("%c", &op) != 1) {
        if (feof(stdin))
            return Eof;
        return Error;
    }

    if (op == '+') {
        if (getchar() == '\n' )
            return Error;
        if (scanf("%99s", productName) != 1 ) {
            return Error;
        }
        if ( getchar() != '\n')
            return Error;
        return Add;
    }

    if (op == '?')
        return ListCnt;

    if (op == '#')
        return ListAll;

    if (op != '?' && op != '#' && op != '\n' && op != '+') {
        return Error;
    }
    return Idle;
}

void reallocArray ( Product** array, int * allocatedSpace ) {
    Product* tmp = (Product*) realloc ( *array, (*allocatedSpace)*2*sizeof(Product));
    if ( tmp == NULL ) {
        free(array);
        exit(-1);
    }
    *array = tmp;
    for ( int i = (*allocatedSpace); i < (*allocatedSpace)*2; ++i ) {
        (*array)[i].soldCnt = 0;
        strcpy ( (*array)[i].name, "");
    }
    (*allocatedSpace) *= 2;
}

void updatePos ( Product* array, int pos ) {
    while ( pos != 0 && array[pos-1].soldCnt < array[pos].soldCnt ) {
        Product tmp = array[pos-1];
        array[pos-1] = array[pos];
        array[pos] = tmp;
        pos--;
    }
}

void insertIntoArray ( char* productName, Product** array, int* productCnt, int* allocatedSpace ) {
    if ( *productCnt == 0 ) {
        strcpy ( (*array)[0].name, productName);
        (*array)[0].soldCnt = 1;
        (*productCnt)++;
        return;
    }

    for ( int i = 0; i < (*productCnt); ++i) {

        int cmp = strcmp ( productName, (*array)[i].name);
        if ( cmp == 0 ) {
            (*array)[i].soldCnt++;
            updatePos ( *array, i );
            return;
        }
    }
    if ( (*productCnt) >= (*allocatedSpace)-1 )
        reallocArray ( array, allocatedSpace );

    (*array)[(*productCnt)].soldCnt = 1;
    strcpy ( (*array)[(*productCnt)].name, productName);
    (*productCnt)++;
}

int printUntilNotSame ( Product* array, int from, int productCnt, int* cntOfProducts ) {
    int cnt = 1;
    int tmp = from;

    while ( tmp < productCnt-1 && array[tmp].soldCnt == array[tmp+1].soldCnt ) {
        cnt++;
        tmp++;
    }

    for ( int i = from; i < from+cnt; ++i ) {
        if ( from+1 == from+cnt)
            printf( "%d. %s, %dx\n", from+1, array[i].name, array[i].soldCnt );
        else
            printf( "%d.-%d. %s, %dx\n", from+1, from+cnt, array[i].name, array[i].soldCnt );
        (*cntOfProducts) += array[i].soldCnt;
    }
    return cnt;
}

int addUntilNotSame ( Product* array, int from, int productCnt, int* cntOfProducts ) {
    int cnt = 1;
    int tmp = from;

    while ( tmp < productCnt-1 && array[tmp].soldCnt == array[tmp+1].soldCnt ) {
        cnt++;
        tmp++;
    }

    for ( int i = from; i < from+cnt; ++i )
        (*cntOfProducts) += array[i].soldCnt;

    return cnt;
}

void printMaxProducts ( Product* array, int productCnt, int numberOfWatched ) {
    int printedProducts = 0;
    int cntOfProducts = 0;
    while ( printedProducts < numberOfWatched && printedProducts < productCnt ) {
        printedProducts += printUntilNotSame( array, printedProducts, productCnt, &cntOfProducts );
    }
    printf ( "Nejprodavanejsi zbozi: prodano %d kusu\n", cntOfProducts );
}

void getMaxCount ( Product* array, int productCnt, int numberOfWatched ) {
    int printedProducts = 0;
    int cntOfProducts = 0;
    while ( printedProducts < numberOfWatched && printedProducts < productCnt ) {
        printedProducts += addUntilNotSame( array, printedProducts, productCnt, &cntOfProducts );
    }
    printf ( "Nejprodavanejsi zbozi: prodano %d kusu\n", cntOfProducts );

}

int main () {
    int numberOfWatched;
    char productName[100];
    int productCnt = 0;
    int allocatedSpace = 200;

    Product * productArray = (Product*)calloc( allocatedSpace, sizeof(Product));

    if ( ! loadNumberOfWatched ( &numberOfWatched ) ) {
        printf("Nespravny vstup.\n");
        free (productArray);
        return 1;
    }

    printf("Pozadavky:\n");
    while ( 1 ) {

        int command = loadCommand ( productName );

        switch ( command ) {
            case Add: {
                insertIntoArray ( productName, &productArray, &productCnt, &allocatedSpace);
                break;
            }

            case ListAll: {
                printMaxProducts ( productArray, productCnt, numberOfWatched );
                break;
            }
            case ListCnt:
                getMaxCount( productArray, productCnt, numberOfWatched );
                break;
            case Error: {
                printf("Nespravny vstup.\n");
                free (productArray);
                return 1;
            }
            case Eof:
                free (productArray);
                return 0;
        }
    }
}