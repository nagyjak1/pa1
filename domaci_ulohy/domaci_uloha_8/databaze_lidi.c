#ifndef __PROGTEST__
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

typedef struct TResult
{
    struct TResult * m_Next;
    int              m_ID;
    char           * m_Name;
} TRESULT;
#endif /* __PROGTEST__ */

typedef struct TPerson
{
    int m_ID;
    char* m_Name;
    struct TPerson * m_Parent1;
    struct TPerson * m_Parent2;
    struct TPerson * m_Child;


} TPERSON;


typedef struct TDatabase
{
    int allocatedSpace;
    int peopleStored;
    struct TPerson** peopleArray;

} TDATABASE;

//-----------------------------------------------------------------------------------------------------------------

void      initAll          ( TDATABASE       * db )
{
    db->allocatedSpace = 100;
    db->peopleStored = 0;
    db->peopleArray = (TPERSON**) calloc ( db->allocatedSpace, sizeof(**db->peopleArray) );
}

//-----------------------------------------------------------------------------------------------------------------

void      doneAll          ( TDATABASE       * db )
{
    for ( int i = 0; i < db->allocatedSpace; ++i )
        free ( (db->peopleArray)[i] );
    free (db->peopleArray);
}

//-----------------------------------------------------------------------------------------------------------------

int       addPerson        ( TDATABASE       * db,
                             int               id,
                             const char      * name,
                             int               id1,
                             int               id2 )
{
    if ( id == 0 || (id1 != 0 && id1 == id2) )
        return 0;

    TPERSON* p1 = NULL, *p2 = NULL;
    for ( int i = 0; i < db->peopleStored; ++i ) {
        if (db->peopleArray[i]->m_ID == id1)
            p1 = db->peopleArray[i];
        if (db->peopleArray[i]->m_ID == id2)
            p2 = db->peopleArray[i];
        if (db->peopleArray[i]->m_ID == id)
            return 0;
    }

    if ( (id1 != 0 && p1 == NULL) || (id2 != 0 && p2 == NULL ) )
        return 0;

    TPERSON* tmp = (TPERSON*) malloc ( sizeof(TPERSON) );
    tmp->m_ID = id;
    tmp->m_Parent1 = p1;
    tmp->m_Parent2 = p2;
    tmp->m_Name = (char*) malloc ( (strlen(name)+1) * sizeof(char) );
    strcpy( tmp->m_Name, name );

    db->peopleArray[db->peopleStored++] = tmp;

    return 1;
}

//-----------------------------------------------------------------------------------------------------------------

void printDatabase ( TDATABASE* db ) {
    for ( int i = 0; i < db->peopleStored; ++i )
        printf("ID: %d \t Name: %s \t 1ParentID: %d \t 2ParentID: %d \n", db->peopleArray[i]->m_ID, db->peopleArray[i]->m_Name, db->peopleArray[i]->m_Parent1==NULL?0:db->peopleArray[i]->m_Parent1->m_ID, db->peopleArray[i]->m_Parent2==NULL?0:db->peopleArray[i]->m_Parent2->m_ID);
    printf("\n");
}

//-----------------------------------------------------------------------------------------------------------------

void printList ( TRESULT* list ) {
    while ( list != NULL ) {
        printf( "ID: %d \t NAME: %s \n", list->m_ID, list->m_Name );
        list = list->m_Next;
    }
}

void listInsert ( TRESULT* l, TPERSON* p ) {
    if ( p == NULL )
        return;

    TRESULT* node = (TRESULT*) malloc (sizeof(TRESULT));
    node->m_ID = p->m_ID;
    node->m_Name = p->m_Name;

    l = node;
    l = l->m_Next;
    listInsert ( l, p->m_Parent1 );

    listInsert ( l, p->m_Parent2 );

}

//-----------------------------------------------------------------------------------------------------------------

TRESULT * ancestors        ( TDATABASE       * db,
                             int               id )
{
    TPERSON* person = NULL;
    for ( int i = 0; i < db->peopleStored; ++i )
        if ( db->peopleArray[i]->m_ID == id ) {
            person = db->peopleArray[i];
            break;
        }

    if ( person == NULL || (person->m_Parent2 == NULL && person->m_Parent1 == NULL) )
        return NULL;

    TRESULT* head = NULL;

    listInsert ( head, person);

    return head;
}

TRESULT * commonAncestors  ( TDATABASE       * db,
                             int               id1,
                             int               id2 )
{
    /* todo */
}
void      freeResult       ( TRESULT         * res )
{
    while ( res != NULL ) {
        TRESULT *tmp = res;
        res = res->m_Next;
        free ( tmp );
    }
}

#ifndef __PROGTEST__
int main                   ( int               argc,
                             char            * argv [] )
{
    char      name[100];
    TDATABASE a, b;
    TRESULT * l;

    initAll ( &a );
    assert ( addPerson ( &a, 1, "John", 0, 0 ) == 1 );
    strncpy ( name, "Jane", sizeof ( name ) );
    assert ( addPerson ( &a, 2, name, 0, 0 ) == 1 );
    assert ( addPerson ( &a, 3, "Caroline", 0, 0 ) == 1 );
    assert ( addPerson ( &a, 4, "Peter", 0, 0 ) == 1 );
    assert ( addPerson ( &a, 5, "George", 1, 2 ) == 1 );
    printDatabase( &a );

    l = ancestors( &a, 5 );
    printList( l );

    doneAll( &a);
/*
    initAll ( &a );
    assert ( addPerson ( &a, 1, "John", 0, 0 ) == 1 );
    strncpy ( name, "Jane", sizeof ( name ) );
    assert ( addPerson ( &a, 2, name, 0, 0 ) == 1 );
    assert ( addPerson ( &a, 3, "Caroline", 0, 0 ) == 1 );
    assert ( addPerson ( &a, 4, "Peter", 0, 0 ) == 1 );
    assert ( addPerson ( &a, 5, "George", 1, 2 ) == 1 );
    assert ( addPerson ( &a, 6, "Martin", 1, 2 ) == 1 );
    assert ( addPerson ( &a, 7, "John", 3, 4 ) == 1 );
    assert ( addPerson ( &a, 8, "Sandra", 3, 4 ) == 1 );
    assert ( addPerson ( &a, 9, "Eve", 1, 2 ) == 1 );
    assert ( addPerson ( &a, 10, "Douglas", 1, 4 ) == 1 );
    strncpy ( name, "Phillipe", sizeof ( name ) );
    assert ( addPerson ( &a, 11, name, 6, 8 ) == 1 );
    strncpy ( name, "Maria", sizeof ( name ) );
    assert ( addPerson ( &a, 12, name, 5, 8 ) == 1 );
    l = ancestors ( &a, 11 );
    assert ( l );
    assert ( l -> m_ID == 1 );
    assert ( ! strcmp ( l -> m_Name, "John" ) );
    assert ( l -> m_Next );
    assert ( l -> m_Next -> m_ID == 2 );
    assert ( ! strcmp ( l -> m_Next -> m_Name, "Jane" ) );
    assert ( l -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_ID == 3 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Name, "Caroline" ) );
    assert ( l -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_ID == 4 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Name, "Peter" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_ID == 6 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "Martin" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_ID == 8 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "Sandra" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
    freeResult ( l );
    assert ( ancestors ( &a, 3 ) == NULL );
    assert ( ancestors ( &a, 13 ) == NULL );
    l = commonAncestors ( &a, 11, 12 );
    assert ( l );
    assert ( l -> m_ID == 1 );
    assert ( ! strcmp ( l -> m_Name, "John" ) );
    assert ( l -> m_Next );
    assert ( l -> m_Next -> m_ID == 2 );
    assert ( ! strcmp ( l -> m_Next -> m_Name, "Jane" ) );
    assert ( l -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_ID == 3 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Name, "Caroline" ) );
    assert ( l -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_ID == 4 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Name, "Peter" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_ID == 8 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "Sandra" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
    freeResult ( l );
    l = commonAncestors ( &a, 10, 9 );
    assert ( l );
    assert ( l -> m_ID == 1 );
    assert ( ! strcmp ( l -> m_Name, "John" ) );
    assert ( l -> m_Next == NULL );
    freeResult ( l );
    assert ( commonAncestors ( &a, 7, 6 ) == NULL );
    l = commonAncestors ( &a, 7, 10 );
    assert ( l );
    assert ( l -> m_ID == 4 );
    assert ( ! strcmp ( l -> m_Name, "Peter" ) );
    assert ( l -> m_Next == NULL );
    freeResult ( l );
    assert ( addPerson ( &a, 13, "Quido", 12, 11 ) == 1 );
    l = ancestors ( &a, 13 );
    assert ( l );
    assert ( l -> m_ID == 1 );
    assert ( ! strcmp ( l -> m_Name, "John" ) );
    assert ( l -> m_Next );
    assert ( l -> m_Next -> m_ID == 2 );
    assert ( ! strcmp ( l -> m_Next -> m_Name, "Jane" ) );
    assert ( l -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_ID == 3 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Name, "Caroline" ) );
    assert ( l -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_ID == 4 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Name, "Peter" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_ID == 5 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "George" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_ID == 6 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "Martin" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_ID == 8 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "Sandra" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_ID == 11 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "Phillipe" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_ID == 12 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "Maria" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
    freeResult ( l );
    l = commonAncestors ( &a, 9, 12 );
    assert ( l );
    assert ( l -> m_ID == 1 );
    assert ( ! strcmp ( l -> m_Name, "John" ) );
    assert ( l -> m_Next );
    assert ( l -> m_Next -> m_ID == 2 );
    assert ( ! strcmp ( l -> m_Next -> m_Name, "Jane" ) );
    assert ( l -> m_Next -> m_Next == NULL );
    freeResult ( l );
    assert ( addPerson ( &a, 1, "Francois", 0, 0 ) == 0 );
    initAll ( &b );
    assert ( addPerson ( &b, 10000, "John", 0, 0 ) == 1 );
    assert ( addPerson ( &b, 10000, "Peter", 0, 0 ) == 0 );
    assert ( addPerson ( &b, 20000, "Jane", 10000, 0 ) == 1 );
    assert ( addPerson ( &b, 30000, "Maria", 10000, 10000 ) == 0 );
    assert ( addPerson ( &b, 40000, "Joe", 10000, 30000 ) == 0 );
    assert ( addPerson ( &b, 50000, "Carol", 50000, 20000 ) == 0 );
    assert ( addPerson ( &b, 12, "Maria", 20000, 10000 ) == 1 );
    l = ancestors ( &a, 12 );
    assert ( l );
    assert ( l -> m_ID == 1 );
    assert ( ! strcmp ( l -> m_Name, "John" ) );
    assert ( l -> m_Next );
    assert ( l -> m_Next -> m_ID == 2 );
    assert ( ! strcmp ( l -> m_Next -> m_Name, "Jane" ) );
    assert ( l -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_ID == 3 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Name, "Caroline" ) );
    assert ( l -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_ID == 4 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Name, "Peter" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_ID == 5 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "George" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_ID == 8 );
    assert ( ! strcmp ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "Sandra" ) );
    assert ( l -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
    freeResult ( l );
    l = ancestors ( &b, 12 );
    assert ( l );
    assert ( l -> m_ID == 10000 );
    assert ( ! strcmp ( l -> m_Name, "John" ) );
    assert ( l -> m_Next );
    assert ( l -> m_Next -> m_ID == 20000 );
    assert ( ! strcmp ( l -> m_Next -> m_Name, "Jane" ) );
    assert ( l -> m_Next -> m_Next == NULL );
    freeResult ( l );
    assert ( ancestors ( &a, 20000 ) == NULL );
    assert ( commonAncestors ( &b, 7, 10 ) == NULL );
    doneAll ( &b );

    doneAll ( &a );
*/
    return 0;
}
#endif /* __PROGTEST__ */
