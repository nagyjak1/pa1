#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int loadShortcut(char **shortcut, int *allocatedLen, int *shortcutLen) {
    printf("Zkratka:\n");
    while (1) {
        if ((*shortcutLen) >= (*allocatedLen)) {
            (*allocatedLen) *= 2;
            (*shortcut) = (char *) realloc((*shortcut), (*allocatedLen) + 1 * sizeof(**shortcut));
        }
        char c = fgetc(stdin);
        if (c == '\n') {
            if ((*shortcutLen) == 0)
                return 0;
            break;
        }
        if (!('A' <= c && c <= 'Z'))
            return 0;
        (*shortcut)[(*shortcutLen)++] = tolower(c);
    }
    (*shortcut)[(*shortcutLen)] = '\0';
    return 1;
}

int loadCommand(int *maxInOneWord, char **string, int *allocatedStringLen) {
    size_t lineSize = 20;
    char *line = (char *) calloc(sizeof(*line), lineSize);
    getline(&line, &lineSize, stdin);

    if (feof(stdin)) {
        free (line);
        return 3;
    }

    char c1, c2;
    int charsRead;
    if (sscanf(line, "%c %d %c%n ", &c1, maxInOneWord, &c2, &charsRead) != 3 || (c1 != '?' && c1 != '#') || c2 != '"' ||
        *maxInOneWord <= 0) {
        free(line);
        return 0;
    }

    int maxStringLen = lineSize - charsRead;
    if (*allocatedStringLen <= maxStringLen) {
        *string = (char *) realloc(*string, (maxStringLen) * sizeof(**string));
        *allocatedStringLen = maxStringLen;
    }
    char* oldLine = line;
    line = line + charsRead;

    charsRead = 0;
    sscanf(line, "%[^\"]%n", *string, &charsRead);

    if ( line[charsRead] != '"'){
        free(oldLine);
        return 0;
    }

    (*string)[charsRead] = '\0';

    for (size_t i = 0; i < strlen(*string); ++i) {
        (*string)[i] = tolower((*string)[i]);
    }

    free(oldLine);
    return c1 == '?' ? 1 : 2;
}

void
findLetterInStringRec(char *string, char *shortcut, int position, int inOneWord, int const maxInOneWord, int *counter,
                      int print) {

    if (*shortcut == '\0') {
        (*counter)++;
        if (print)
            printf("\"%s\"\n", string);
        return;
    }

    for (size_t i = position; i < strlen(string); ++i) {
        if (string[i] == ' ')
            inOneWord = 0;

        if (string[i] == *shortcut)
            if (inOneWord != maxInOneWord) {
                string[i] = toupper(string[i]);
                findLetterInStringRec(string, shortcut + 1, i + 1, inOneWord + 1, maxInOneWord, counter, print);
                string[i] = tolower(string[i]);
                continue;
            }
    }
}


int main() {
    int shortcutLen = 0, shortcutAllocatedLen = 2;
    char *shortcut = (char *) malloc(shortcutAllocatedLen * sizeof(*shortcut));

    if (!loadShortcut(&shortcut, &shortcutAllocatedLen, &shortcutLen)) {
        printf("Nespravny vstup.\n");
        free(shortcut);
        return 1;
    }

    int maxInOneWord = 0, allocatedStringLen = 30;
    char *string = (char *) malloc(allocatedStringLen * sizeof(*string));
    printf("Problemy:\n");

    while (1) {
        int command = loadCommand(&maxInOneWord, &string, &allocatedStringLen);
        switch (command) {
            case 1: { // ?
                int counter = 0;
                findLetterInStringRec(string, shortcut, 0, 0, maxInOneWord, &counter, 1);
                printf("> %d\n", counter);
                break;
            }

            case 2: {// #
                for (int i = 1; i <= maxInOneWord; ++i) {
                    int counter = 0;
                    findLetterInStringRec(string, shortcut, 0, 0, i, &counter, 0);
                    printf("> limit %d: %d\n", i, counter);
                }
                break;
            }
            case 3:
                free(shortcut);
                free(string);
                return 0;

            default:
                printf("Nespravny vstup.\n");
                free(shortcut);
                free(string);
                return 1;
        }
    }
}
