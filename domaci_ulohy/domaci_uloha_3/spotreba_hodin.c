#ifndef __PROGTEST__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#endif /* __PROGTEST__ */


#define CHANGES_PER_DAY 292886

//----------------------------------------------------------------------------------------------------------------------

int digitChangeCost(int newNumber) {
    switch (newNumber) {
        case 0:
        case 3:
            return 2;
        case 1:
        case 8:
            return 4;
        case 2:
        case 7:
            return 5;
        case 4:
        case 5:
            return 3;
        case 6:
        case 9:
            return 1;
    }
    return 0;
}

//----------------------------------------------------------------------------------------------------------------------

long long int changesPerTime(int h1, int m1, int h2, int m2) {
    long long int changes = 0;
    int days = 0;

    while (((days * 24 * 60) + h1 * 60 + m1) != (h2 * 60 + m2)) {
        m1++;
        changes += 200;

        if (m1 % 60 == 0) {

            h1++;
            if (h1 % 10 == 0)
                changes += digitChangeCost(h1 / 10);

            if (h1 % 24 == 0) {
                changes += 6;
                days++;
                h1 = 0;
            } else
                changes += digitChangeCost(h1 % 10);

            m1 = 0;
            changes += 5;

        } else {
            if (m1 % 10 == 0) {
                changes += digitChangeCost(m1 / 10);
            }
            changes += digitChangeCost(m1 % 10);
        }

    }
    return changes;
}

//----------------------------------------------------------------------------------------------------------------------

int isLeap(int year) {
    int isLeap = 0;
    if (year % 4000 == 0)
        isLeap = 0;
    else if (year % 400 == 0)
        isLeap = 1;
    else if (year % 100 == 0)
        isLeap = 0;
    else if (year % 4 == 0)
        isLeap = 1;
    return isLeap;
}

//----------------------------------------------------------------------------------------------------------------------

long long int countLeapYears(int month, int year) {
    if (month <= 2)
        year--;
    return (year / 4 - year / 100 + year / 400 - year / 4000);
}

//----------------------------------------------------------------------------------------------------------------------

int daysInMonth(int month, int year) {
    int days = 0;
    switch (month) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            days = 31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            days = 30;
            break;
        case 2:
            if (isLeap(year))
                days = 29;
            else
                days = 28;
            break;
    }
    return days;
}

//----------------------------------------------------------------------------------------------------------------------

int dateIsValid(int year, int month, int day, int hour, int minute) {

    if (year < 1600 || month < 1 || month > 12 || hour < 0 || hour > 23 || day < 1 ||
        day > daysInMonth(month, year) || minute < 0 || minute > 59)
        return 0;
    return 1;
}

//----------------------------------------------------------------------------------------------------------------------

long long int countDays(long long int y1, int m1, int d1, long long int y2, int m2, int d2) {
    long long int days1, days2;
    days1 = (y1 * 365) + d1 + countLeapYears(m1, y1);
    for (int i = 1; i < m1; i++)
        days1 += daysInMonth(i, 2001);

    days2 = (y2 * 365) + d2 + countLeapYears(m2, y2);
    for (int i = 1; i < m2; i++)
        days2 += daysInMonth(i, 2001);

    return days2 - days1;
}

//----------------------------------------------------------------------------------------------------------------------

int energyConsumption(int y1, int m1, int d1, int h1, int i1,
                      int y2, int m2, int d2, int h2, int i2, long long int *consumption) {

    if (!dateIsValid(y1, m1, d1, h1, i1) || !dateIsValid(y2, m2, d2, h2, i2))
        return 0;

    if ((countDays(y1, m1, d1, y2, m2, d2) < 0) ||
        (countDays(y1, m1, d1, y2, m2, d2) == 0 && h1 > h2) ||
        (countDays(y1, m1, d1, y2, m2, d2) == 0 && h1 == h2 && i1 > i2))
        return 0;

    if (i1 > i2) {
        h2--;
        i2 += 60;
    }
    if (h1 > h2) {
        d2--;
        h2 += 24;
    }

    long long int wholeDays = countDays(y1, m1, d1, y2, m2, d2);

    *consumption = (wholeDays * CHANGES_PER_DAY + changesPerTime(h1, i1, h2, i2));

    return 1;
}

//----------------------------------------------------------------------------------------------------------------------

#ifndef __PROGTEST__

int main(int argc, char *argv[]) {
    long long int consumption;

    assert(!isLeap(1901));
    assert(!isLeap(1902));
    assert(!isLeap(1903));
    assert(!isLeap(1905));
    assert(isLeap(1904));
    assert(isLeap(1908));
    assert(isLeap(1996));
    assert(isLeap(2004));
    assert(!isLeap(1700));
    assert(!isLeap(1800));
    assert(!isLeap(1900));
    assert(!isLeap(2100));
    assert(isLeap(1600));
    assert(isLeap(2000));
    assert(isLeap(2400));
    assert(isLeap(3600));
    assert(isLeap(4400));
    assert(!isLeap(4000));
    assert(!isLeap(8000));
    assert(isLeap(2800));
    assert(!isLeap(2100));
    assert(!isLeap(2700));
    assert(isLeap(2180));
    assert(isLeap(2104));
    assert(isLeap(2768));
    assert(isLeap(2588));
    assert(isLeap(2208));
    assert(isLeap(2184));
    assert(isLeap(1904));
    assert(!isLeap(1900));
    assert(isLeap(2020));

    assert(dateIsValid(2000, 2, 29, 12, 0));
    assert(!dateIsValid(2000, 1, 32, 12, 0));
    assert(!dateIsValid(2000, 0, 29, 12, 0));
    assert(!dateIsValid(2000, 2, 29, 12, 60));
    assert(!dateIsValid(2000, 2, 29, 25, 0));
    assert(!dateIsValid(1599, 2, 29, 12, 0));
    assert(dateIsValid(2000, 3, 31, 12, 0));
    assert(dateIsValid(2000, 4, 30, 12, 0));
    assert(!dateIsValid(2000, 4, 31, 12, 0));
    assert(dateIsValid(2000, 2, 29, 23, 59));
    assert(dateIsValid(2000, 1, 1, 1, 1));

    assert(energyConsumption(2021, 10, 1, 13, 15,
                             2021, 10, 1, 18, 45, &consumption) == 1
           && consumption == 67116LL);
    assert(energyConsumption(2021, 10, 1, 0, 0,
                             2021, 10, 1, 12, 0, &consumption) == 1
           && consumption == 146443LL);
    assert(energyConsumption(2021, 10, 1, 0, 15,
                             2021, 10, 1, 0, 25, &consumption) == 1
           && consumption == 2035LL);
    assert(energyConsumption(2021, 10, 1, 12, 0,
                             2021, 10, 1, 12, 0, &consumption) == 1
           && consumption == 0LL);
    assert(energyConsumption(2021, 10, 1, 12, 0,
                             2021, 10, 1, 12, 1, &consumption) == 1
           && consumption == 204LL);


    assert(energyConsumption(2021, 11, 1, 12, 0,
                             2021, 10, 1, 12, 0, &consumption) == 0);
    assert(energyConsumption(2021, 10, 32, 12, 0,
                             2021, 11, 10, 12, 0, &consumption) == 0);
    assert(energyConsumption(2100, 2, 29, 12, 0,
                             2100, 2, 29, 12, 0, &consumption) == 0);
    assert(energyConsumption(2400, 2, 29, 12, 0,
                             2400, 2, 29, 12, 0, &consumption) == 1
           && consumption == 0LL);


    assert(energyConsumption(2021, 10, 1, 13, 15,
                             2021, 10, 1, 18, 45, &consumption) == 1
           && consumption == 67116LL);
    assert(energyConsumption(2021, 10, 1, 13, 15,
                             2021, 10, 2, 11, 20, &consumption) == 1
           && consumption == 269497LL);

    assert(energyConsumption(2021, 10, 1, 11, 20,
                             2021, 10, 2, 11, 20, &consumption) == 1
           && consumption == CHANGES_PER_DAY);


    assert(energyConsumption(2021, 1, 1, 13, 15,
                             2021, 10, 5, 11, 20, &consumption) == 1
           && consumption == 81106033LL);
    assert(energyConsumption(2024, 1, 1, 13, 15,
                             2024, 10, 5, 11, 20, &consumption) == 1
           && consumption == 81398919LL);

    assert(energyConsumption(1900, 1, 1, 13, 15,
                             1900, 10, 5, 11, 20, &consumption) == 1
           && consumption == 81106033LL);

    assert(energyConsumption(2015, 4, 25, 23, 45,
                             2015, 4, 25, 23, 30, &consumption) == 0);

    energyConsumption(2150, 6, 27, 3, 57,
                      204035081, 10, 26, 8, 45, &consumption);


    assert(energyConsumption(2150, 6, 27, 3, 57,
                             204035081, 10, 26, 8, 45, &consumption) == 1
           && consumption == 21826288500912381);

    return 0;
}

#endif /* __PROGTEST__ */
