#include <stdio.h>

int loadInput(int *x, int *y) {
    int read = scanf("%d %d", x, y);
    if (read != 2 || *x < 0 || *y > 36 || *y < 2) {
        if (!feof(stdin) || (feof(stdin) && read == 1)) {
            printf("Nespravny vstup.\n");
            return 1;
        } else
            return 1;
    }
    return 0;
}

int countDigits(int number, int system) {
    int counter = 1;
    while (number / system != 0) {
        number = number / system;
        counter++;
    }
    return counter;
}

char valueToChar(int number) {
    if (number < 10)
        return '0' + number;
    else
        return 'a' + number - 10;
}

void printDecimalNumberInSystem(int number, int system) {
    int index = 0;
    char result[100];
    do {
        result[index++] = valueToChar(number % system);
        number /= system;
    } while (number != 0);
    for (int i = index - 1; i >= 0; --i)
        printf("%c", result[i]);
    printf("\n");
}

void findNumber(int position, int system) {
    int i = 0, digitsCounter = 0, digitPosition, digitsInNumber;

    while (1) {
        digitsInNumber = countDigits(i, system);
        digitsCounter += digitsInNumber;
        if (digitsCounter > position) {
            digitPosition = digitsInNumber + position - digitsCounter;
            break;
        }
        ++i;
    }

    printDecimalNumberInSystem(i, system);

    for (int j = 0; j < digitsInNumber; ++j) {
        if (j == digitPosition) {
            printf("^\n");
            break;
        }
        printf(" ");
    }
}

int main() {
    int position;
    int system;
    printf("Pozice a soustava:\n");
    while (!loadInput(&position, &system))
        findNumber(position, system);
    return 1;
}
