#include <stdio.h>

int main() {
    int choice = 0;
    printf("ml' nob:\n");
    if (scanf("%d", &choice) != 1)
        choice = 0;
    const char* mess;
    switch (choice) {
        case 1:
            mess = "Qapla'\n"
                   "noH QapmeH wo' Qaw'lu'chugh yay chavbe'lu' 'ej wo' choqmeH may' DoHlu'chugh lujbe'lu'.\n";
            break;
        case 2:
            mess = "Qapla'\n"
                   "Qu' buSHa'chugh SuvwI', batlhHa' vangchugh, qoj matlhHa'chugh, pagh ghaH SuvwI''e'.\n";
            break;
        case 3:
            mess = "Qapla'\n"
                   "qaStaHvIS wa' ram loS SaD Hugh SIjlaH qetbogh loD.\n";
            break;
        case 4:
            mess = "Qapla'\n"
                   "Ha'DIbaH DaSop 'e' DaHechbe'chugh yIHoHQo'.\n";
            break;
        case 5:
            mess = "Qapla'\n"
                   "leghlaHchu'be'chugh mIn lo'laHbe' taj jej.\n";
            break;
        default:
            printf("luj\n");
            return 1;
    }
    printf("%s", mess);
    return 0;
}
