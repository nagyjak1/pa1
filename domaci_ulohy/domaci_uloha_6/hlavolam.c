#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEFAULT_LINE_SIZE 3

//----------------------------------------------------------------------------------------------------------------------

void freeMatrix(char **matrix, int rowCnt) {
    for (int i = 0; i < rowCnt; ++i)
        free(matrix[i]);
    free(matrix);
}

//----------------------------------------------------------------------------------------------------------------------

void reallocMatrix(char ***matrix, const int newRowCnt, const int newColumnCnt, int *oldRowCnt, int *oldColumnCnt) {

    // realokace hlavnich pointeru - radky v matici
    char **tmpMatrix = (char **) realloc(*matrix, newRowCnt * sizeof(**matrix));
    if (tmpMatrix == NULL) {
        freeMatrix(*matrix, *oldRowCnt);
        exit(1);
    }
    *matrix = tmpMatrix;

    // nove pridane radky
    for (int i = *oldRowCnt; i < newRowCnt; ++i)
        (*matrix)[i] = (char *) malloc((newColumnCnt + 1) * sizeof((**matrix)[0]));

    // realokace sloupecku
    for (int i = 0; i < newRowCnt; ++i) {
        char *tmpRow = (char *) realloc((*matrix)[i], (newColumnCnt + 1) * sizeof((**matrix)[0]));
        if (tmpRow == NULL) {
            freeMatrix(*matrix, newRowCnt);
            exit(1);
        }
        (*matrix)[i] = tmpRow;
    }
    *oldRowCnt = newRowCnt;
    *oldColumnCnt = newColumnCnt;
}

//----------------------------------------------------------------------------------------------------------------------

int loadInput(char ***matrix, int *matrixSize, int *loadedSize) {

    char *line = (char *) malloc(DEFAULT_LINE_SIZE * sizeof(*line));
    int firstLineSize = 0, linesCount = 0, charsRead;
    size_t lineSize = DEFAULT_LINE_SIZE;
    printf("Hlavolam:\n");

    while (1) {
        charsRead = getline(&line, &lineSize, stdin);

        // podle delky prvni nactene radky nastav velikost vstupu a pripadne zmenit velikost matice
        if (firstLineSize == 0) {
            firstLineSize = charsRead;
            *loadedSize = firstLineSize - 1;
            if (charsRead > *matrixSize)
                reallocMatrix(matrix, lineSize, lineSize, matrixSize, matrixSize);
        }

        // vice nebo mene znaku nez prvni radek nebo pocet radku udelal obdelnik
        if ((charsRead != -1 && charsRead != firstLineSize) || linesCount >= firstLineSize) {
            free(line);
            return 0;
        }

        // nastav obsah radku matice podle nactene line
        for (int i = 0; i < charsRead - 1; ++i) {
            (*matrix)[linesCount][i] = line[i];
        }
        linesCount++;

        // cteni se nepodarilo - dosel vstup
        if (charsRead == -1) {
            free(line);
            if (linesCount != firstLineSize)
                return 0;
            else
                return 1;
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

int doesWordFit(int matrixSize, int wordLen, int x, int y, int deltaX, int deltaY) {
    if (((x + (deltaX * wordLen) >= 0) && (x + (deltaX * wordLen)) < matrixSize) &&
        ((y + (deltaY * wordLen) >= 0) && (y + (deltaY * wordLen)) < matrixSize))
        return 1;
    return 0;
}

//----------------------------------------------------------------------------------------------------------------------

int cmpfunc ( const void* a, const void* b ) {
    return ( strcmp ( *(const char**)a, *(const char**)b ) );
}

void findAllWordsWithGivenLen(char **matrix, int matrixSize, int wordLen, int* foundWordsCount, char*** foundWordsArray, int* stringArrayCapacity, char* word ) {
    int rowNbr[] = {-1, -1, -1, 0, 0, 1, 1, 1};
    int colNbr[] = {-1, 0, 1, -1, 1, -1, 0, 1};

    for (int i = 0; i < matrixSize; ++i) {
        for (int j = 0; j < matrixSize; ++j) {
            for (int dir = 0; dir < 8; ++dir) {
                int deltaX = rowNbr[dir];
                int deltaY = colNbr[dir];

                if (doesWordFit(matrixSize, wordLen - 1, i, j, deltaX, deltaY)) {
                    for (int k = 0; k < wordLen; ++k) {
                        word[k] = matrix[i + k * deltaX][j + k * deltaY];
                    }
                    word[wordLen] = '\0';

                    if (*foundWordsCount >= *stringArrayCapacity) {
                        (*foundWordsArray) = (char**) realloc ( (*foundWordsArray), (*stringArrayCapacity)*2 * sizeof(*(*foundWordsArray)));
                        for ( int h = (*stringArrayCapacity); h < (*stringArrayCapacity)*2; h++ )
                            (*foundWordsArray)[h] = (char*) malloc ( (matrixSize+1) * sizeof( *(*foundWordsArray)[0]) );
                        *stringArrayCapacity *= 2;
                    }
                    strcpy ((*foundWordsArray)[(*foundWordsCount)++], word);
                }
                if (wordLen == 1)
                    break;
            }
        }
    }
    qsort ( (*foundWordsArray), (*foundWordsCount), sizeof((*foundWordsArray)[0]), cmpfunc);
}

//----------------------------------------------------------------------------------------------------------------------

void testMatrix(char **matrix, int matrixSize) {
    int wordCnt, allocatedWords = 400, duplicityCount = 0, duplicityAllocatedCount = 100;

    char** duplicities = (char**) malloc ( duplicityAllocatedCount * sizeof(  *duplicities ));
    for ( int i = 0; i < duplicityAllocatedCount; ++i) {
        duplicities[i] = (char*) malloc ( (matrixSize+1) * sizeof( *duplicities[0]));
    }

    char** wordsArray = (char**) malloc ( allocatedWords * sizeof(  *wordsArray ));
    for ( int i = 0; i < allocatedWords; ++i) {
        wordsArray[i] = (char*) malloc ( (matrixSize+1) * sizeof( *wordsArray[0]));
    }

    char *word = (char *) malloc((matrixSize + 1) * sizeof(*word));

    for (int i = matrixSize; i > 0; --i) {

        wordCnt = 0;

        findAllWordsWithGivenLen(matrix, matrixSize, i, &wordCnt, &wordsArray, &allocatedWords, word);

        for (int i = 0; i < wordCnt - 1; ++i) {
            if (strcmp(wordsArray[i], wordsArray[i + 1]) == 0 ) {

                if (duplicityCount >= duplicityAllocatedCount) {
                    duplicities = (char **) realloc(duplicities, duplicityAllocatedCount * 2 * sizeof(*duplicities));
                    for (int h = duplicityAllocatedCount; h < duplicityAllocatedCount * 2; h++)
                        duplicities[h] = (char *) malloc((matrixSize + 1) * sizeof(*duplicities[0]));
                    duplicityAllocatedCount *= 2;
                }
                strcpy ( duplicities[duplicityCount++], wordsArray[i] );
                while ( i < wordCnt-1 && strcmp(wordsArray[i], wordsArray[i+1]) == 0 )
                    ++i;
            }
        }
        if (duplicityCount != 0)
            break;
    }

    if ( duplicityCount == 0 )
        printf("Zadne opakujici se slovo.\n");
    else {
        printf("Nejdelsi opakujici se slova:\n");
        for (int i = 0; i < duplicityCount; ++i) {
            printf("%s\n", duplicities[i]);
        }
    }

    for (int i = 0; i < allocatedWords; ++i)
        free(wordsArray[i]);
    free(wordsArray);

    for ( int i = 0; i < duplicityAllocatedCount; ++i )
        free ( duplicities[i] );
    free ( duplicities );

    free ( word );
}

//----------------------------------------------------------------------------------------------------------------------

int main() {

    int matrixSize = 200;
    int loadedSize = 0;
    char **matrix = (char **) malloc(matrixSize * sizeof(*matrix));
    for (int i = 0; i < matrixSize; ++i)
        matrix[i] = (char *) malloc(matrixSize * sizeof(*matrix[i]));

    if (!loadInput(&matrix, &matrixSize, &loadedSize)) {
        printf("Nespravny vstup.\n");
        freeMatrix(matrix, matrixSize);
        return 1;
    }

    testMatrix(matrix, loadedSize);

    freeMatrix(matrix, matrixSize);
    return 0;
}

